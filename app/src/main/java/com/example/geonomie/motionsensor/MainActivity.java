package com.example.geonomie.motionsensor;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity implements SensorEventListener {
    private SensorManager mSensorManager;
    private Intent intent;

    private Sensor mSensor;
    private long lastUpdate;
    private boolean color = false;
    private int mStep=0;
    private View accelView;
    private TextView _accelerometerTextView;
    private TextView _gyroscopeTextView;
    private TextView _magneticfieldTextView;
    private TextView _uncalibratedmagneticfieldTextView;
    private TextView _LineAccelrometerTextView;
    private TextView _stepDetector;

    private long mLastAccelTimestamp;
    private DeadReckoning deadReckoning;

    private long mLastActAccelTimestamp;
    private long mLastGyroTimestamp;
    private long mLastGravityTimestamp;
    private long mLastMagneticFieldTimestamp;
    private long mLastRotationVectorTimestamp;

    private float[] mDisplacement = new float[] { 0.f, 0.f, 0.f };
    private float[] mVelocity = new float[] { 0.f, 0.f, 0.f };
    private float[] mAccel = new float[] { 0.f, 0.f, 0.f };
    private float[] mActAccel = new float[] { 0.f, 0.f, 0.f };
    private float[] mAngularVelocity = new float[] { 0.f, 0.f, 0.f };
    private float[] mAngles = new float[] { 0.f, 0.f, 0.f };
    private float[] mGravity = new float[] { 0.f, 0.f, 0.f };
    private float[] mMagneticField = new float[] { 0.f, 0.f, 0.f };
    private float[] mRotationVector = new float[] { 0.f, 0.f, 0.f };
    private float[] mTrueAccel = new float[] { 0.f, 0.f, 0.f};
    private float[] mI = new float[] { 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f,
            0.f };
    private float[] mR = new float[] { 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f,
            0.f };
    private float[] mRV = new float[] { 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f,
            0.f };

    private float[] mPrevAccel = new float[] { 0.f,0.f,0.f};
    private float[] mPrevActAccel = new float[] { 0.f,0.f,0.f};
    private float[] mPrevTrueAccel = new float[] {0.f, 0.f,0.f};
    private float[] mPrevDisplacement = new float[]{0.f,0.f,0.f};
    private float[] mPrevVelocity = new float[] {0.f,0.f,0.f};

    private double a_x = 0, a_y = 0, a_z = 0;
    private double g_x = 0, g_y = 0, g_z = 0;
    private double m_x = 0, m_y = 0, m_z = 0;
    private double moy_x = 0, moy_y = 0, moy_z = 0;
    private double um_x = 0, um_y = 0, um_z = 0;
    private double moy_mag = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        deadReckoning=new DeadReckoning();

        intent=new Intent(this, BroadcastService.class);
        mSensorManager=(SensorManager)getSystemService(Context.SENSOR_SERVICE);
        mSensor=mSensorManager.getDefaultSensor(Sensor.TYPE_ALL);
        lastUpdate = System.currentTimeMillis();
        _accelerometerTextView=findViewById(R.id.acclerometer);
        _LineAccelrometerTextView=findViewById(R.id.lineaccelerometer);
        _gyroscopeTextView=findViewById(R.id.gyroscope);
        _magneticfieldTextView=findViewById(R.id.magnetometer);
        _uncalibratedmagneticfieldTextView=findViewById(R.id.uncalibratedmagnetometer);
        _stepDetector=findViewById(R.id.StepDetector);

    }

    private BroadcastReceiver broadcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateUI(intent);
        }
    };

    private void updateUI(Intent intent) {
        float mstartx = deadReckoning.getmStartX();
        float mstarty = deadReckoning.getmStartY();
        float[] mlocation = deadReckoning.getLocation();
        float mcurrentx = mlocation[0];
        float mcurrenty = mlocation[1];
        int msteps = deadReckoning.getStepCount();
       // float orientation=deadReckoning.get

        TextView startx = (TextView) findViewById(R.id.StartX);
        TextView starty = (TextView) findViewById(R.id.StartY);
        TextView currentx = (TextView) findViewById(R.id.CurrentX);
        TextView currenty = (TextView) findViewById(R.id.CurrentY);
        TextView stepcount = (TextView) findViewById(R.id.StepCount);

        startx.setText("Start X"+Float.toString(mstartx));
        starty.setText("Start Y"+Float.toString(mstarty));
        currentx.setText("Current X"+Float.toString(mcurrentx));
        currenty.setText("Current Y"+Float.toString(mcurrenty));
        stepcount.setText("Steps "+Integer.toString(msteps));

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        synchronized (this){
            long deltaT=sensorEvent.timestamp;
            if (sensorEvent.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
                if (mLastActAccelTimestamp==0){
                    mLastActAccelTimestamp=sensorEvent.timestamp;
                }
                deltaT-=mLastAccelTimestamp;
                mLastActAccelTimestamp=sensorEvent.timestamp;
                mPrevActAccel=mActAccel;
//                mActAccel=sensorEvent.values.clone();
                System.arraycopy(sensorEvent.values, 0, mActAccel, 0, 3);

                deadReckoning.onAccelUpdate(mActAccel,deltaT,mLastActAccelTimestamp);
                _accelerometerTextView.setText("Acceleromter :"+sensorEvent.values[0]+","+sensorEvent.values[1]+", "+sensorEvent.values[2]+".");
            }

            if (sensorEvent.sensor.getType()==Sensor.TYPE_LINEAR_ACCELERATION){
                if (mLastAccelTimestamp==0){
                    mLastActAccelTimestamp=sensorEvent.timestamp;
                }

                deltaT-=mLastAccelTimestamp;
                mLastAccelTimestamp=sensorEvent.timestamp;
                mPrevAccel=mAccel;
                // copy new magnetometer data into magnet array
                System.arraycopy(sensorEvent.values, 0, mAccel, 0, 3);
                mAccel=sensorEvent.values.clone();
                updateTrueAccel();
                deadReckoning.onLinearAccelUpdate(mAccel, deltaT, mLastAccelTimestamp);
                _LineAccelrometerTextView.setText("LineAccelerometer :"+sensorEvent.values[0]+","+sensorEvent.values[1]+", "+sensorEvent.values[2]+".");

            }
            if (sensorEvent.sensor.getType()==Sensor.TYPE_STEP_DETECTOR){

                if (sensorEvent.values[0]==1.0f)
                {
                    mStep++;
                }
                _stepDetector.setText("Step Detector :"+mStep);

            }

            if (sensorEvent.sensor.getType()==Sensor.TYPE_MAGNETIC_FIELD){
                if (mLastMagneticFieldTimestamp==0){
                    mLastMagneticFieldTimestamp=sensorEvent.timestamp;
                }
                deltaT-=mLastMagneticFieldTimestamp;
                mLastMagneticFieldTimestamp=sensorEvent.timestamp;
                System.arraycopy(sensorEvent.values, 0, mMagneticField, 0, 3);
                deadReckoning.onMagneticFieldUpdate(mMagneticField, deltaT, mLastMagneticFieldTimestamp);
                _magneticfieldTextView.setText("Magnetometer :"+sensorEvent.values[0]+","+sensorEvent.values[1]+", "+sensorEvent.values[2]+".");
            }
            if (sensorEvent.sensor.getType()==Sensor.TYPE_GYROSCOPE){
                if (mLastGyroTimestamp == 0) {
                    mLastGyroTimestamp = sensorEvent.timestamp;
                }
                deltaT -= mLastGyroTimestamp;
                mLastGyroTimestamp = sensorEvent.timestamp;
                System.arraycopy(sensorEvent.values, 0, mAngularVelocity, 0, 3);
                deadReckoning.onGyroUpdate(mAngularVelocity, deltaT, mLastGyroTimestamp);
                _gyroscopeTextView.setText("Gyroscope :"+sensorEvent.values[0]+","+sensorEvent.values[1]+", "+sensorEvent.values[2]+".");
            }

            if (sensorEvent.sensor.getType()==Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED){
                _uncalibratedmagneticfieldTextView.setText("UncalibratedMagneticfield :"+sensorEvent.values[0]+","+sensorEvent.values[1]+", "+sensorEvent.values[2]+".");
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
    @Override
    protected void onResume(){
        super.onResume();
        mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION),SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE),SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED),SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this,mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR),SensorManager.SENSOR_DELAY_NORMAL);
        startService(intent);
        registerReceiver(broadcastReceiver,new IntentFilter(BroadcastService.BROADCAST_ACTION));
    }

    @Override
    protected void onPause(){
        super.onPause();
        mSensorManager.unregisterListener(this,mSensorManager   .getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
        mSensorManager.unregisterListener(this,mSensorManager   .getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION));
        mSensorManager.unregisterListener(this,mSensorManager   .getDefaultSensor(Sensor.TYPE_GYROSCOPE));
        mSensorManager.unregisterListener(this,mSensorManager   .getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD));
        mSensorManager.unregisterListener(this,mSensorManager   .getDefaultSensor(Sensor.TYPE_STEP_DETECTOR));
        unregisterReceiver(broadcastReceiver);
        stopService(intent);

    }

    private void updateTrueAccel() {
        mPrevTrueAccel = mTrueAccel.clone();
        mTrueAccel[0] = mRV[0] * mAccel[0] + mRV[1] * mAccel[1] + mRV[2]
                * mAccel[2];
        mTrueAccel[1] = mRV[3] * mAccel[0] + mRV[4] * mAccel[1] + mRV[5]
                * mAccel[2];
        mTrueAccel[2] = mRV[6] * mAccel[0] + mRV[7] * mAccel[1] + mRV[8]
                * mAccel[2];
    }

}
